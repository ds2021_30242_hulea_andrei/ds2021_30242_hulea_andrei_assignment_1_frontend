import Navbar from "react-bootstrap/Navbar";
import {Nav} from "react-bootstrap";
import React from "react";
import './Menu.css'

const Menu = () =>{

    return(

        <div>
            <ul>
                <li style={{fontSize:'20px',marginRight:'10px'}}><a href="/admin/user"> Energy platform </a></li>

                <li style={{marginTop:'3px'}}><a href="/admin/user"> Users </a></li>
                <li style={{marginTop:'3px'}}><a href="/admin/devices"> Devices </a></li>
                <li style={{marginTop:'3px'}}><a href="/admin/sensors"> Sensors </a></li>

                <li
                    style={{float:'right',marginTop:'3px'}}

                ><a href="/login"> Log out </a></li>
            </ul>

        </div>
    )


}
export default Menu;