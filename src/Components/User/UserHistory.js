import React from 'react'
import UserMenu from "../Menu/UserMenu";
import axiosInstance from "../../axios";
import {DataGrid} from "@mui/x-data-grid";
import {Button, Container, TextField} from "@material-ui/core";
import * as SockJS from "sockjs-client";
import * as Stomp from "stompjs";

const monitoredValuesColumns = [
    {field: 'id', headerName: 'Id', width: 100},
    {field: 'timestamp', headerName: 'Timestamp', width: 300},
    {field: 'value', headerName: 'Value', width: 125},
]

export default class UserHistory extends React.Component{



    componentDidMount() {
        this.connect();
    }

    connect(){
        const websocket = new SockJS("http://localhost:8080/socket")
        const stompClient = Stomp.over(websocket);

        stompClient.connect({}, frame =>{
            console.log(frame)
            stompClient.subscribe("/topic/socket/notif/" + localStorage.getItem("USER_ID"), notification => {
                alert(notification.body)
            })
        })
    }



    constructor() {
        super();
        this.state = {
            monitoredValues:[],
            devices:[],
            devicesString:"",
            totalSum:0
        }

        axiosInstance
            .get("/user/devices/sensors/monitored/" + localStorage.getItem("USER_ID"))
            .then(response => {
                this.setState({monitoredValues:response.data})


            })
            .catch(error => {
                console.log(error);
            })

        axiosInstance
            .get("/user/sum/" + localStorage.getItem("USER_ID"))
            .then(response => {
                this.setState({totalSum:response.data})
            })
            .catch(error => {
                console.log(error);
            })

    }

    handleInput = event => {

        const { value,name } = event.target;
        this.setState(
            {
                [name]: value
            }
        );
    };

    render(){

        return(

            <div>
                <UserMenu/>
                <div className="split">

                    <Container >

                    <h4 style={{
                        display:'flex',
                        justifyContent:'center',
                        marginTop:'80px'
                    }}>Sensor values history</h4>
                    <div style={{display: 'flex', justifyContent: 'center',
                        alignItems: 'center', marginTop: '20px'}}>
                        <div style={{display: 'flex', height: 500, width: 1750}}>
                            <div style={{flexGrow: 5}}>
                                <DataGrid
                                    on rows={this.state.monitoredValues}
                                    columns={monitoredValuesColumns}
                                    pageSize={15}
                                    autoHeight="true"
                                />
                            </div>
                        </div>
                    </div>

                    </Container>


                    <Container>
                        <h4 style={{
                            display:'flex',
                            justifyContent:'center',
                            marginTop:'80px'
                        }}>Total consumption:</h4>

                        <h1 style={{
                            display:'flex',
                            justifyContent:'center',
                            marginTop:'80px'
                        }}>{this.state.totalSum}</h1>

                    </Container>
                </div>
            </div>
        )

    }

}